# -*- coding: utf-8 -*-
import cv2
import numpy as np


def soglia(img, soglia):
    img_new = img
    dim_row, dim_col = img.shape
    print "soglia in corso"
    for i in range(0, dim_row):
        for j in range(0, dim_col):
            val = img[i][j]
            if (val > soglia):
                val = 255
            else:
                val = 0
            img_new[i][j] = val
    print "soglia finito"
    cv2.imshow('Soglia Window', img_new)
    cv2.waitKey(1000 * 5)
    return img_new;