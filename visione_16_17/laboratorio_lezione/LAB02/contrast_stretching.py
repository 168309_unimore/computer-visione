# -*- coding: utf-8 -*-
import cv2
import numpy as np

def contrast_stretching(img):
    img_new = img
    minimo = np.min(img)
    maximum = np.max(img)
    dim_row, dim_col = img.shape
    print "Stretching in corso"
    for i in range(0, dim_row):
        for j in range(0, dim_col):
            old_val = img[i][j]
            val = ((old_val - minimo)*255 / (maximum - minimo))
            if (val > 255):
                val = 255
            if(val < 0):
                val = 0
            img_new[i][j] = val
    print "Stretching finito"
    cv2.imshow('Contrast Stretching', img_new)
    cv2.waitKey(3000)
    return img_new

def contrast_stretching_prof(img):
    img_new = img
    minimo = np.min(img)
    maximum = np.max(img)
    print "Stretching in corso"
    img_new = ((img - minimo)*255) /(maximum - minimo)
    print "Stretching finito"
    cv2.imshow('Contrast Stretching Prof', img_new.astype(np.uint8))
    cv2.waitKey(1000 * 5)
    return img_new