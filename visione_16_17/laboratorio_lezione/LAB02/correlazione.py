# -*- coding: utf-8 -*-
import cv2
import numpy as np

def exec_kernel_on_image (small_img, kernel):
    dim_row, dim_col = small_img.shape
    val = 0;
    for k in range(0, dim_row):
        for l in range(0, dim_col):
            val += small_img[k][l] * kernel[k][l]
    if(val > 255):
        return np.uint8(255);
    if(val < 0):
        return np.uint8(0);
    return np.uint8(val);

def correlazione(img, kernel):
    img_new = img
    dim_row, dim_col = img.shape
    dim_kernel_row, dim_kernel_col = kernel.shape
    print "correlazione in corso"
    ''' se la dim del kernel e 3 allora il bordo e = 1 se la dim del
     kernel 5 allora il bordoer e 2 il numero e sempre disparti del kernel
     k = dimensione del kernel ed k e sempre quadrato'''

    colonne_k =int(dim_kernel_row/2);
    colonne_k_plus = colonne_k + 1;

    for i in range(colonne_k, dim_row - colonne_k):
        for j in range(colonne_k, dim_col -colonne_k):
            arr =  np.empty((0, dim_kernel_row))
            for l in range((-1 * colonne_k), colonne_k_plus): # colonne_k+1 perchè non e compreso
                temp = np.array([], np.uint8)
                for k in range((-1 * colonne_k), colonne_k_plus):
                    temp = np.append(temp, img[i + l][j + k]);
                arr = np.append(arr, [temp], axis=0)
            #prime_vector = np.array([img[i-1][j-1], img[i-1][j], img[i-1][j+1]]) ;
            #second_vector = np.array([img[i][j-1], img[i][j], img[i][j+1]]) ;
            #thrid_vector = np.array([img[i+1][j-1], img[i+1][j], img[i+1][j+1]]) ;
            #main_vec = np.array([prime_vector, second_vector, thrid_vector]) ;

            img_new[i][j] = exec_kernel_on_image (arr, kernel);
    print "correlazione finito"
    return img_new;