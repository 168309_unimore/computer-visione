# -*- coding: utf-8 -*-
import cv2
import numpy as np
import matplotlib.pyplot as plt

'''
Negativo e una operazione lineare
'''

def negative_image(img):
    k = 255 #k is the offset constant often called also bias or brightness
    s = -1 # s is the scale factor, often called also gain or contrast
    img_new = img
    M, N = img.shape
    for i in range(0, M):
        for j in range(0, N):
            val = s * img[i][j] + k
            val =  np.uint8(val);
            img_new[i][j] = np.uint8(val)
    return img_new


def variazione_luminanza(img, num_image):
    k = 0 #k is the offset constant often called also bias or brightness
    s = 1 # s is the scale factor, often called also gain or contrast
    M, N = img.shape
    for l in range (0, num_image):
        img_new = img
        for i in range(0, M):
            for j in range(0, N):
                img_new[i][j] = (s + l) * img[i][j] + k
        cv2.imwrite("image" + np.str_(l) + ".jpg", img_new)



#img = cv2.imread('img/org.jpg', cv2.CV_LOAD_IMAGE_GRAYSCALE)
#neg_img = negative_image(img)
#cv2.imshow('Negative Image', neg_img)
#cv2.waitKey(1000 * 5)


#dim_images = 10;
#img = cv2.imread('img/org.jpg', cv2.CV_LOAD_IMAGE_GRAYSCALE)
#images = variazione_luminanza(img, dim_images)

sin_img = np.zeros((3, 3), np.uint8)
neg_img = negative_image(sin_img)
cv2.imwrite("image11.jpg", neg_img)