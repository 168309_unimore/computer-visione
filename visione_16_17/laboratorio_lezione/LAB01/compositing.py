# -*- coding: utf-8 -*-
import cv2
import numpy as np
import matplotlib.pyplot as plt

'''
THRESHOLDING (soglia) e una operazione puntale:
g(x,y) = {1 : f(x, y) >= T, 0 : otherwise} g(x, y) e il nuovo pixel, sè f(x, y) e maggiore di T (cioè da una certa solgia)
allora il valore e 1 altrimenti 0
'''
#
'''
esercizio di compositing exploit (sfruttando) la soglia possiamo comporre due immagini,
le funzione da creare
compose(f_img, b_img, low_threshold, high_threshold)
f_img = immagine da qui predenre il forground pixel,
b_img = immagine da qui prendere il background pixel
low_threshold la soglia bassa
high_threshold soglia alta

dobbiamo cercare una soglia per divivdere l'immagine in modo da prendere il minion.
e nel altra immagine prednere solo il background.
soglia la scegliamo come OTSU
'''
'''
SOLUZIONE
Loop over each foreground image pixel
• If the pixel falls between the two thresholds, put the corresponding
background pixel in the output image
• Otherwise, put the forward pixel

'''
