# -*- coding: utf-8 -*-
import cv2
import numpy as np
import matplotlib.pyplot as plt

from histogram import *

'''
devo dire in quale bin del istogramma deve andare un certo byte sè ho 256 bin allora ho un bin per ogni byte
quindi avro un vettore di dimensione di 256 un per ogni byte del colore, tutto  il  vettore viene inizialiato a zero
e devo contare le occorenze di tutti i byte.

sè i bin del istogramma sono pochi allora devo calcoalre come un certo numero di range al interno dei bin

esempio sè ho 4 bin allora
in ogni bin andranno n_numero di byte 256 / 4 dove = 64

0 - 63 andranno nel primo bin
64 -127 andranno nel primo bin
128 -191 andranno nel primo bin
192 -255 andranno nel primo bin

cioè il vettore di bin avra la dimensione 4

nel indice 0 andranno tutti i byte che sono simile al range 0 - 63
nel indice 1 andranno tutti i byte che sono simile al range 64 -127
nel indice 2 andranno tutti i byte che sono simile al range 128 -191
nel indice 3 andranno tutti i byte che sono simile al range 192 -255

problema: come faccio a dire che il byte 150 deve andare nel bin 2 perche indice parte dal 0

soluzione: dividio
1) 150 / 64 => 2
2) 128 / 64 => 2
3) 127 / 64 => 1
3) 63 / 64  =>  0
'''

def hist_1d(img, n_bins):
    vec = np.zeros(n_bins, dtype=np.uint8)
    max_divide = 256/n_bins
    for row in img:
        for col in row:
            temp_index  = col //max_divide
            temp_index = temp_index - 1
            if(temp_index == -1):
                temp_index = 0
            vec[temp_index] =  vec[temp_index] + 1
    return vec

def hist(img, n_bins):
    vec = np.zeros(n_bins, dtype=np.uint8)
    max_divide = 256/n_bins
    for row in img:
        for col in row:
            temp_index  = col //max_divide
            temp_index = temp_index - 1
            if(temp_index == -1):
                temp_index = 0
            vec[temp_index] =  vec[temp_index] + 1
    draw_img_and_hist(img, vec);

def rgb2single_color(img):
    M, N, c = img.shape
    r = np.zeros((M, N), np.uint8)
    g = np.zeros((M, N), np.uint8)
    b = np.zeros((M, N), np.uint8)
    for i in range(0, M):
        for j in range(0, N):
            b[i][j] = img[i][j][0]
            g[i][j] = img[i][j][1]
            r[i][j] = img[i][j][2]

    img_new = np.array([r, g, b])
    return img_new

def elaboration_histogramma_per_ogni_colore(r, n_bins):
    print "Separate RGB  Channel"
    print "Elaboration For Red Channel"
    hist_r = hist_1d(r[0], n_bins)
    print "Elaboration For Green Channel"
    hist_g = hist_1d(r[1], n_bins)
    print "Elaboration For Blue Channel"
    hist_b = hist_1d(r[2], n_bins)
    return [hist_r, hist_g, hist_b]

def print_histogramma_per_ogni_colore(img, r, hist, n_bins):
    x_bar = np.tile((np.arange(n_bins) * 255 / n_bins), 0)
    f, axarr = plt.subplots(3, 3)
    axarr[0][1].imshow(img)

    axarr[1][0].imshow(r[0])
    axarr[1][1].imshow(r[1])
    axarr[1][2].imshow(r[2])

    axarr[2][0].bar(np.arange(hist[0].size), hist[0])
    axarr[2][0].set_xticks(np.arange(hist[0].size))
    axarr[2][0].set_xticklabels(x_bar)

    axarr[2][1].bar(np.arange(hist[1].size), hist[1])
    axarr[2][1].set_xticks(np.arange(hist[1].size))
    axarr[2][1].set_xticklabels(x_bar)

    axarr[2][2].bar(np.arange(hist[2].size), hist[2])
    axarr[2][2].set_xticks(np.arange(hist[2].size))
    axarr[2][2].set_xticklabels(x_bar)

    plt.show()

#img = cv2.imread('img/org.jpg', 1)
#cv2.imshow('tt', img)
#cv2.waitKey(1000*5)
#cv2.waitKey(1)
#hist(img, 10)

n_bins = 255
img = cv2.imread('img/sea.jpg', cv2.CV_LOAD_IMAGE_COLOR)
print "Separate Color"
r = rgb2single_color(img)
print "Initial Elaboration for Histogram"
hist = elaboration_histogramma_per_ogni_colore(r, n_bins)
print "Initial Printing"
print_histogramma_per_ogni_colore(img, r, hist, n_bins);