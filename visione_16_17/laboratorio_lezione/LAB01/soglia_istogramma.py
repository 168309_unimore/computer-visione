# -*- coding: utf-8 -*-
import cv2
import numpy as np
import matplotlib.pyplot as plt
#from histogram import *


def draw_img_and_hist(img, histogram):
    '''
    algortimo del prof per visualizzare istogramma e immagine in base ai bins del istogramma
    :param img:
    :param histogram:
    :return:
    '''
    if img.ndim == 3:
        h, w, c = img.shape
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # for visualization
        cmap = None
    elif img.ndim == 2:
        h, w, c = img.shape + (1,)
        cmap = 'gray'
    else:
        raise Exception('img has weird number of dimensions: {}'.format(img.ndim))

    assert histogram.size % c == 0, 'Histogram bins are not a multiple of image channels. I consider this wrong.'
    n_bins = histogram.size // c
    x_bar = np.tile((np.arange(n_bins) * 255 / n_bins), reps=(c,))

    f, axarr = plt.subplots(2, 1)
    axarr[0].imshow(img, cmap=cmap)
    axarr[1].bar(np.arange(histogram.size), histogram)
    axarr[1].set_xticks(np.arange(histogram.size))
    axarr[1].set_xticklabels(x_bar)

    plt.show()


def print_threshold_imges(img, img_new):
    '''
    stampa le due immagini a livello di grigio la prima originale grey level,
    e la secondo immagine elaborata con la soglia
    :param img:
    :param img_new:
    :return:
    '''
    f, axarr = plt.subplots(2, 1)
    cmap = "gray"
    axarr[0].imshow(img, cmap=cmap)
    axarr[1].imshow(img_new, cmap=cmap)
    plt.show()


def create_istogramma(img):
    '''
    crearo semplicemente un istogramma con 256 bins che vanno da 0 a 255
    il valore di ritorno e un vettore con la frequenza di ogni pixel
    :param img:
    :return:
    '''
    M, N = img.shape
    hist = np.zeros(256, np.uint32)
    for i in range(0, M):
        for j in range(0, M):
            hist[img[i][j]]  = hist[img[i][j]] + 1
    return hist


def get_threshold_max(hist):
    '''
    prendo indice del valore massimo del istogramma l'indice e il pixel di soglia
    il valore di ritorno e semplicemente una soglia
    :param hist:
    :return:
    '''
    return np.argmax(hist, axis=None, out=None)


def get_threshold_avg(img):
    '''
    media dei pixel diviso la dimensione della immagine
    il valore di ritorno e la soglia
    :param img:
    :return:
    '''
    dim_row, dim_col = img.shape
    sum = 0
    for i in range(0, dim_row):
        for j in range(0, dim_col):
            sum += img[i][j]

    val = sum / (dim_row * dim_col)

    return np.uint8(val)


def get_threshold_median(img, hist):
    dim_row, dim_col = img.shape
    sum = 0
    for i in range(0, dim_row):
        for j in range(0, dim_col):
            sum += img[i][j]

    val = sum / (dim_row * dim_col)

    return np.uint8(val)


def threshold(hist, soglia):
    '''
    creò una nuova immagine utilizzando la vecchia e uso la soglia per creare una nuova immagine
    in cui il valore del pixel maggiore della soglia avrà il valore 1 altrimenti avrà il valore 0
    :param img:
    :param soglia:
    :return:
    '''
    img_new = img.copy()
    dim_row, dim_col = img.shape
    print "soglia in corso"
    for i in range(0, dim_row):
        for j in range(0, dim_col):
            val = img[i][j]
            if (val <= soglia ): # posso usare anche un range
                img_new[i][j] = 0
            else:
                img_new[i][j] = 1
    print "soglia finito"
    print_threshold_imges(img, img_new)


def k_means_soglia(hist, soglia):
    '''
    per istogramma gia fatto e lo divido in due classi usando la soglia poi faccio la media signolarmente e il
    risultato di tutti e due li divido per due e ottengo una nuova soglia, lo faccio finche la variazione della soglia
    cambia o cambia di poco
     una soglia di partenza casuale o media o la mediana
    da cui posso partire e genrare una algortimo di tipo k-means
    la soglia viene applicata sui pixel
    :param img:
    :param soglia:
    :return:
    '''

    hist1 = np.zeros(soglia)
    hist2 = np.array(256 - soglia)
    N, M, C = img.shape
    sum_hist1 = 0
    sum_hist2 = 0
    new_soglia = 0
    for i in range(0, soglia):
        sum_hist1 += hist1[i]

    for i in range(soglia, 256):
        sum_hist2 += hist2[i]

    new_soglia = (sum_hist1 + sum_hist2)/2


img = cv2.imread('img/org1.jpg', 0)
#cv2.imshow('preview', img)
#cv2.waitKey(1000 * 1)
hist = create_istogramma(img)
draw_img_and_hist(img, hist)
#soglia = get_threshold_max(hist)
#soglia  = soglia + 20
soglia = get_threshold_avg(img)
print soglia
threshold(img, soglia)
